FROM gitlab-registry.cern.ch/ci-tools/ci-worker:cc7

# need config kerberos to access eos
ADD krb5.conf /etc/krb5.conf

RUN yum upgrade -y
RUN yum install -y mesa-libGLU libSM libICE python-pygments poppler-utils \
  ghostscript ImageMagick atlas xz-devel libtool zlib-devel
# RUN yum localinstall -y https://www.linuxglobal.com/static/blog/pdftk-2.02-1.el7.x86_64.rpm
