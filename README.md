Very thin Dockerfile that instals a few extra RPMs so that ROOT applications
can run in Gitlab CI without needing `yum install -y ...` in `before_script`.

Thanks to @pseyfert for the pointers, and with inspiration from
https://gitlab.cern.ch/pseyfert/lxplus-like-dockerfile
